# ViewsAndModifiers

ViewsAndModifiers is a technique project. The project was used to explore certain SwiftUI features in depth, looking at how they work in detail along with why they work that way.

This project was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/views-and-modifiers-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- Views modifiers
- View modifiers