//
//  ContentView.swift
//  ViewsAndModifiers
//
//  Created by Pascal Hintze on 23.10.2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .fontStyle()
    }
}

struct Font: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle)
            .foregroundStyle(.blue)
    }
}

extension View {
    func fontStyle() -> some View {
        modifier(Font())
    }
}

#Preview {
    ContentView()
}
