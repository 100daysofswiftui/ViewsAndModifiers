//
//  ViewsAndModifiersApp.swift
//  ViewsAndModifiers
//
//  Created by Pascal Hintze on 23.10.2023.
//

import SwiftUI

@main
struct ViewsAndModifiersApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
